//
//  PetStore.swift
//  hi-assign6
//
//  Created by Student on 2016-11-08.
//  Copyright © 2016 Student. All rights reserved.
//
//
// Project:      assign-6-2016
// File:         PetStore.swift
// Author:       Ismael F. Hepp
// Date:         Nov 08, 2016
// Course:       IMG204
//

import Foundation

class PetStore {
  private let TOTAL_ARRAYS = 4
  
  private var walkers:[Animal] = []
  private var cleaners:[Animal] = []
  private var eaters:[Animal] = []
  private var cagers:[Animal] = []
  
  private var walker_index:Int
  private var cleaner_index:Int
  private var eater_index:Int
  private var cager_index:Int
  
  init(){
    walker_index = 0
    cleaner_index = 0
    eater_index = 0
    cager_index = 0
  }
  
  func createRandom(max:Int)
  {
    walker_index = 0
    cleaner_index = 0
    eater_index = 0
    cager_index = 0
    
    let total = max * 4
    
    for var i in 1...total {
      let name:String = Generator.getPetName()
      let age:Int = Generator.getPetAge()
      let breed:String = Generator.getPetBreed()
      
      // create an animal
      var animal:Animal = Animal(name: "", age: -1)
      if breed == "Dog" {
        animal = Dog(name: name, age: age, breed: breed)
      } else if (breed == "Cat") {
        animal = Cat(name: name, age: age, breed: breed)
      } else if (breed == "Bird") {
        animal = Bird(name: name, age: age, breed: breed)
      } else {
        animal = Fish(name: name, age: age, breed: breed)
      }
      
      if i <= total/4 {
        walkers.append(animal)
      } else if i <= total/2 {
        cleaners.append(animal)
      } else if i <= total*3/4 {
        eaters.append(animal)
      } else {
        cagers.append(animal)
      }

    }
  }
  
  func getNextWalker() -> Animal {
    let tmpAnimal = walkers[walker_index]
    walker_index = (walker_index + 1) % walkers.count
    return tmpAnimal
  }
  
  func getNextCleaner() -> Animal {
    let tmpAnimal = cleaners[cleaner_index]
    cleaner_index = (cleaner_index + 1) % cleaners.count
    return tmpAnimal
  }
  
  func getNextEater() -> Animal {
    let tmpAnimal = eaters[eater_index]
    eater_index = (eater_index + 1) % eaters.count
    return tmpAnimal
  }
  
  func getNextCager() -> Animal {
    let tmpAnimal = cagers[cager_index]
    cager_index = (cager_index + 1) % cagers.count
    return tmpAnimal
  }
  
  func getPreviousWalker() -> Animal {
    let tmpAnimal: Animal
    if walker_index - 1 < 0 {
      walker_index = walkers.count - 1
    } else {
      walker_index = walker_index - 1
    }
    if walker_index - 1 < 0 {
      tmpAnimal = walkers[walkers.count - 1]
    } else {
      tmpAnimal = walkers[walker_index - 1]
    }
    return tmpAnimal
  }
  
  func getPreviousCleaner() -> Animal {
    let tmpAnimal: Animal
    if cleaner_index - 1 < 0 {
      cleaner_index = cleaners.count - 1
    } else {
      cleaner_index = cleaner_index - 1
    }
    if cleaner_index - 1 < 0 {
      tmpAnimal = cleaners[cleaners.count - 1]
    } else {
      tmpAnimal = cleaners[cleaner_index - 1]
    }
    return tmpAnimal
  }
  
  func getPreviousEater() -> Animal {
    let tmpAnimal: Animal
    if eater_index - 1 < 0 {
      eater_index = eaters.count - 1
    } else {
      eater_index = eater_index - 1
    }
    if eater_index - 1 < 0 {
      tmpAnimal = eaters[eaters.count - 1]
    } else {
      tmpAnimal = eaters[eater_index - 1]
    }
    return tmpAnimal
  }

  func getPreviousCager() -> Animal {
    let tmpAnimal: Animal
    if cager_index - 1 < 0 {
      cager_index = cagers.count - 1
    } else {
      cager_index = cager_index - 1
    }
    if cager_index - 1 < 0 {
      tmpAnimal = cagers[cagers.count - 1]
    } else {
      tmpAnimal = cagers[cager_index - 1]
    }
    return tmpAnimal
  }
  
  
  
  
}

