//
//  Bird.swift
//  hi-assign6
//
//  Created by Student on 2016-11-08.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Bird: Animal, Cageable {
  var birdBreed:String
  
  init(name:String, age:Int, breed: String)
  {
    birdBreed = breed
    super.init(name:name, age:age)
    
  }
  
  override var description:String {
    return "Bird breed = \(birdBreed)" + super.description
  }
  
  func cage() {
    print("You put \(name)")
  }
  
  func cleanEnclousure(){
    print("You cleaned \(name)'s cage")
  }
  
}

