//
//  Fish.swift
//  hi-assign6
//
//  Created by Student on 2016-11-08.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Fish: Animal, NeedTank {
  var fishBreed:String
  
  init(name:String, age:Int, breed: String)
  {
    fishBreed = breed
    super.init(name:name, age:age)
    
  }
  
  override var description:String {
    return "Fish breed = \(fishBreed)" + super.description
  }
  
  func putInTank() {
    print("You put \(name) in a tank")
  }
  
  func cleanENclousure() {
    print("You cleaned \(name)'s tank")
  }
  
}

