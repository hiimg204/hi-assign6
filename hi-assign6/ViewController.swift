//
//  ViewController.swift
//  hi-assign6
//
//  Created by Student on 2016-11-08.
//  Copyright © 2016 Student. All rights reserved.
//
// Project:      assign-6-2016
// File:         ViewController.swift
// Author:       Ismael F. Hepp
// Date:         Nov 08, 2016
// Course:       IMG204
//

import Cocoa

class ViewController: NSViewController {

  @IBOutlet weak var labelFeed: NSTextField!
  @IBOutlet weak var labelWalk: NSTextField!
  @IBOutlet weak var labelCage: NSTextField!
  @IBOutlet weak var labelClean: NSTextField!
  
  var petStore : PetStore = PetStore()
  
  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.
    petStore.createRandom(max: 10)
    labelFeed.stringValue = petStore.getNextEater().description
    labelWalk.stringValue = petStore.getNextWalker().description
    labelCage.stringValue = petStore.getNextCager().description
    labelClean.stringValue = petStore.getNextCleaner().description
  }

  override var representedObject: Any? {
    didSet {
    // Update the view, if already loaded.
    }
  }

  @IBAction func reload(_ sender: AnyObject) {
    petStore = PetStore()
    petStore.createRandom(max: 10)
    labelFeed.stringValue = petStore.getNextEater().description
    labelWalk.stringValue = petStore.getNextWalker().description
    labelCage.stringValue = petStore.getNextCager().description
    labelClean.stringValue = petStore.getNextCleaner().description
  }
  
  @IBAction func previous(_ sender: AnyObject) {
    labelFeed.stringValue = petStore.getNextEater().description
    labelWalk.stringValue = petStore.getNextWalker().description
    labelCage.stringValue = petStore.getNextCager().description
    labelClean.stringValue = petStore.getNextCleaner().description
  }
  
  @IBAction func next(_ sender: AnyObject) {
    labelFeed.stringValue = petStore.getPreviousEater().description
    labelWalk.stringValue = petStore.getPreviousWalker().description
    labelCage.stringValue = petStore.getPreviousCager().description
    labelClean.stringValue = petStore.getPreviousCleaner().description
  }
  

}

