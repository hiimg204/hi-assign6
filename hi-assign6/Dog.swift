//
//  Dog.swift
//  hi-assign6
//
//  Created by Student on 2016-11-08.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Dog: Animal, Walkable {
  var dogBreed:String
  
  init(name:String, age:Int, breed: String)
  {
    dogBreed = breed
    super.init(name:name, age:age)
    
  }
  
  override var description:String {
    return "Dog breed = \(dogBreed)" + super.description
  }
  
  func exercise() {
    print("You took \(name) for a walk")
  }
  
  
}

