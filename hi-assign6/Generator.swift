//
//  Generator.swift
//  hi-assign6
//
//  Created by Student on 2016-11-08.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

public class Generator
{
  public static let petNames: [String] = ["Alex", "Ben", "Cecile", "Dan", "Eva",
                                          "Felix", "Gomez", "Hanna", "Ivan"]
  
  public static let petAges: [Int] = [1,2,3,4,5,6,7,8,9,10,11,12]
  
  
  public static let petBreed: [String] = ["Dog", "Cat", "Bird", "Firsh" ]
  
  
  public class func getPetName() -> String
  {
    let count:Int = petNames.count
    let rindex:Int = Int(arc4random_uniform(UInt32(count))) % count
    return petNames[rindex]
  }
  
  public class func getPetAge() -> Int
  {
    let count:Int = petAges.count
    let rindex:Int = Int(arc4random_uniform(UInt32(count))) % count
    return petAges[rindex]
  }
  
  public class func getPetBreed() -> String
  {
    let count:Int = petBreed.count
    let rindex:Int = Int(arc4random_uniform(UInt32(count))) % count
    return petBreed[rindex]
  }
  
}
