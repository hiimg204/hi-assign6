//
//  Cat.swift
//  hi-assign6
//
//  Created by Student on 2016-11-08.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Cat: Animal, Walkable {
  var catBreed:String
  
  init(name:String, age:Int, breed: String)
  {
    catBreed = breed
    super.init(name:name, age:age)
    
  }
  
  override var description:String {
    return "Cat breed = \(catBreed)" + super.description
  }
  
  func exercise() {
    print("You took \(name) for a walk")
  }
}
