//
//  Animal.swift
//  hi-assign6
//
//  Created by Student on 2016-11-08.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Animal: CustomStringConvertible {
  var name:String
  var age:Int
  
  init(name:String, age:Int)
  {
    self.name = name
    self.age = age
  }
  
  var description: String {
    return " Name = \(name) Age = \(age)"
  }
  
  func feed() {
    print("\(name) is fed.")
  }
  
}

