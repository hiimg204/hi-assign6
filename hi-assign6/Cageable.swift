//
//  Cageable.swift
//  hi-assign6
//
//  Created by Student on 2016-11-08.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

protocol Cageable {
  func cage()
  func cleanEnclousure()
}
